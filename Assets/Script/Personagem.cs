using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personagem : MonoBehaviour
{
    public float Speed;
    public float JumpForce;
    private bool isGround = true;

    public Rigidbody2D rigidbody2D;
    public Animator animator;

    const float layerGround = 8;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>(); 
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
    }

    void Move() 
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f);
        transform.position += movement * Time.deltaTime * Speed;

        //Andando pra Direita
        if(Input.GetAxisRaw("Horizontal") > 0f)
        {
            animator.SetBool("walk", true);
            //Rotaciona o boneco pra Direita
            transform.eulerAngles = new Vector3(0f, 0f, 0f);
        } else 
        //Andando pra Esquerda
        if(Input.GetAxisRaw("Horizontal") < 0f)
        {
            animator.SetBool("walk", true);
            //Rotaciona o boneco pra Esquerda
            transform.eulerAngles = new Vector3(0f, 180f, 0f);
        } 
        else
        {
            animator.SetBool("walk", false);
        }  
    }

    void Jump()
    {
        if(Input.GetKeyDown(KeyCode.Space) && isGround){
            rigidbody2D.AddForce(new Vector2(0f, JumpForce), ForceMode2D.Impulse);

            animator.SetBool("jump", true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("isGround") || collision.gameObject.layer == layerGround){
            isGround = true;
            animator.SetBool("jump", false);
            
        }

        if(collision.gameObject.CompareTag("Dead")){
            Destroy(gameObject);
        }

        if(collision.gameObject.CompareTag("plataforma")){
            this.transform.parent = collision.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("isGround") || collision.gameObject.layer == layerGround){
            isGround = false;
        } 

        if(collision.gameObject.CompareTag("plataforma")){
            this.transform.parent = null;
        }      
    }

    private void  OnTriggerEnter2D(Collider2D collider){
        if (collider.tag.Equals("Apple")) {
            Placar.scoreValue++;
            Destroy(collider.gameObject);
        }
    }

}
